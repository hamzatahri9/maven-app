def incrementVersion(){
    echo 'incrementing app version...'
                sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                def version = matcher[0][1]
                env.IMAGE_NAME = "$version-$BUILD_NUMBER"
}
def cleanJars(){
    echo "cleaning builds"
    sh "mvn clean package"
}

def buildJar() {
    echo "building the application..."
    sh 'mvn package'
}


def buildImage() {
        echo "building the docker image..."
                    withCredentials([usernamePassword(credentialsId: 'Docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                sh '''
                    docker build -t tahri9/demo-app:${IMAGE_NAME} .
                    echo $PASS | docker login -u $USER --password-stdin
                    docker push tahri9/demo-app:${IMAGE_NAME}
                '''
                    }
}

def deployImage() {
    echo "deploying the application..."
    echo "Done automatically"

}

def commitChanges() {
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'git config user.email --global "jenkins@example.com"'
        sh 'git config user.name  --global "jenkins" '
        sh 'git status'
        sh 'git branch'
        sh 'git config --list'

        // Set the remote URL (no need to do it every time)
        withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            sh '''
                git remote set-url origin https://${USER}:${PASS}@gitlab.com/hamzatahri9/maven-app.git
            ''' 
        }

        sh 'git add .'
        sh 'git commit -m "ci: version bump"'

        // Push the changes back to the main branch
    }
}




return this